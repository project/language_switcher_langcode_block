CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Maintainers

INTRODUCTION
------------

This module provides a new language switcher block that displays a list of links
using the language code as link title.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/language_switcher_langcode_block

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/language_switcher_langcode_block

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

MAINTAINERS
-----------

Current maintainers:
 * Balis Matzouranis (balis_m) - https://www.drupal.org/user/2664243

This project has been sponsored by:
 * Pelagus Creative Web
   Pelagus Creative Web is a Drupal web agency that offers a variety of services
   including Web Design, Development, Promotion and Hosting. Our team consists
   of passionate Drupal developers, designers, content editors, project managers
   and system administrators. Visit https://www.pelagus.gr for more information.
