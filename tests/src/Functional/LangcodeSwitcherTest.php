<?php

namespace Drupal\Tests\language_switcher_langcode_block\Functional;

use Drupal\Core\Language\LanguageInterface;
use Drupal\Tests\BrowserTestBase;

/**
 * Functional tests for the langcode switcher.
 */
class LangcodeSwitcherTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'locale',
    'language',
    'block',
    'language_switcher_langcode_block',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'classy';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create user.
    $user = $this->drupalCreateUser([
      'administer blocks',
      'administer languages',
      'access administration pages',
    ]);

    // Log in user.
    $this->drupalLogin($user);
  }

  /**
   * Functional tests for the language switcher langcode block.
   */
  public function testLanguageSwitcherLangcodeBlock() {
    // Add language.
    $edit = ['predefined_langcode' => 'el'];
    $this->drupalGet('admin/config/regional/language/add');
    $this->submitForm($edit, 'Add language');

    // Enable URL language detection and selection.
    $edit = ['language_interface[enabled][language-url]' => '1'];
    $this->drupalGet('admin/config/regional/language/detection');
    $this->submitForm($edit, 'Save settings');

    // Enable the language switcher langcode block.
    $block = $this->drupalPlaceBlock('language_switcher_langcode_block:' . LanguageInterface::TYPE_INTERFACE, [
      'id' => 'test_language_switcher_langcode_block',
      'label' => 'Language switcher (langcode)',
    ]);

    // Assert that the language switcher langcode block is displayed on the
    // frontpage.
    $this->drupalGet('');
    $this->assertSession()->pageTextContains($block->label());

    // Assert that the language switcher langcode block has the appropriate
    // language links texts.
    $block_list_items = $this->xpath('//div[@id=:id]/ul/li', [':id' => 'block-test-language-switcher-langcode-block']);
    $block_list_items_links_texts = [];
    foreach ($block_list_items as $block_list_item) {
      $block_list_item_link = $block_list_item->find('xpath', 'a');
      $block_list_items_links_texts[] = $block_list_item_link->getText();
    }
    $this->assertSame(['en', 'el'], $block_list_items_links_texts);
  }

}
