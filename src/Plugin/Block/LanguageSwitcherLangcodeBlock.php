<?php

namespace Drupal\language_switcher_langcode_block\Plugin\Block;

use Drupal\language\Plugin\Block\LanguageBlock;

/**
 * Provides a 'Language switcher langcode' block.
 *
 * @Block(
 *   id = "language_switcher_langcode_block",
 *   admin_label = @Translation("Language switcher (langcode)"),
 *   category = @Translation("Multilingual"),
 *   deriver = "Drupal\language_switcher_langcode_block\Plugin\Derivative\LanguageSwitcherLangcodeBlock"
 * )
 */
class LanguageSwitcherLangcodeBlock extends LanguageBlock {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = parent::build();

    if (!empty($build['#links'])) {
      foreach ($build['#links'] as $langcode => &$link) {
        $link['attributes']['title'] = $link['title'];
        $link['title'] = $langcode;
      }
    }

    return $build;
  }

}
